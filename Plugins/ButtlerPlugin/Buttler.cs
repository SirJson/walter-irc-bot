﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Meebey.SmartIrc4net;
using Walter.Core;

namespace ButtlerPlugin
{
	struct ItemMessage
	{
		public string GiveMessage;
		public string PostMessage;
		public string Name;
	}

	public class Buttler : Module
	{
		List<ItemMessage> ItemCard = new List<ItemMessage>()
		{
			new ItemMessage {Name = "Apfel", GiveMessage = "gibt {0} einen Apfel", PostMessage = "Ihr Apfel Sir. Höchste Qualität so wie sie es verlangt haben"},
			new ItemMessage {Name = "Apfelsaft", GiveMessage = "gibt {0} ein Glas Apfelsaft", PostMessage = "Ihr frisch gepresster Apfelsaft Sir"},
			new ItemMessage {Name = "Apfelkuchen", GiveMessage = "gibt {0} ein Stück Apfelkuchen", PostMessage = "Ihr Apfelkuchen Sir. Passen sie auf er ist noch etwas warm"},
			new ItemMessage {Name = "Kaffee", GiveMessage = "serviert {0} einen Kaffee", PostMessage = "Ihren Kaffee Sir, natürlich mit extra viel Koffein"},
			new ItemMessage {Name = "Cola", GiveMessage = "gibt {0} eine Cola", PostMessage = "Ihre Cola Sir"},
			new ItemMessage {Name = "Wein", GiveMessage = "schenkt {0} einen teuren Wein ein", PostMessage = "Ihr Glas Wein Sir"},
			new ItemMessage {Name = "Whiskey", GiveMessage = "serviert {0} Whiskey aus einem Eichenfass", PostMessage = "Ihren Whiskey Sir wie sie ihn bestellt haben"},
			new ItemMessage {Name = "Limonade", GiveMessage = "serviert {0} eine Limonade", PostMessage = "Ihre Limonade Sir"},
			new ItemMessage {Name = "Bier", GiveMessage = "serviert {0} eine Flasche Export Bier", PostMessage = "Ihr Bier Sir"},
			new ItemMessage {Name = "Pizza", GiveMessage = "serviert {0} eine frische Pizza", PostMessage = "Ihre Pizza Sir, ich habe sie aus dem Ausland Importieren lassen"},
			new ItemMessage {Name = "Kaviar", GiveMessage = "serviert {0} russischen Kaviar", PostMessage = "Ihren Kaviar Sir, so wie sie ihn bestellt haben"},
		};

		public override void Init()
		{
			Flags = ModuleFlags.UseChannelMessageEvent | ModuleFlags.UseOnJoinEvent;
		}

		public override void OnJoin(JoinEventArgs data)
		{
			BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Who, "Willkommen in " + data.Channel + ", " + data.Who);
		}

		public override void OnChannelMessage(IrcMessageData data)
		{
			if (data.Message.StartsWith("!gib"))
			{
				string cmd = data.Message.Substring(5).ToLower();
				Console.WriteLine(cmd);
				if (cmd == "servierliste")
				{
					string message = "Ich kann ihnen: ";
					int i = 0;
					foreach (ItemMessage item in ItemCard)
					{
						if (i == ItemCard.Count - 1)
							message += "und " + item.Name;
						else
							message += item.Name + ", ";
						i++;
					}
					message += " anbieten";
					BotMain.Instance.IRC.SendMessage(SendType.Message, data.Nick, message);
				}
				else
				{
					foreach (ItemMessage item in ItemCard)
					{
						if (cmd == item.Name.ToLower())
						{
							BotMain.Instance.IRC.SendMessage(SendType.Action, data.Channel, String.Format(item.GiveMessage, data.Nick));
							BotMain.Instance.IRC.SendMessage(SendType.Message, data.Channel, item.PostMessage);
						}
					}
				}
			}
		}

	}
}
