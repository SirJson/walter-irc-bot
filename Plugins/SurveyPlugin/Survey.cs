﻿using System;
using System.Collections.Generic;
#if MONO
using Mono.Data.Sqlite;
#else
using System.Data.SQLite;
#endif
using System.Linq;
using System.Text;
using Meebey.SmartIrc4net;
using Walter.Core;

namespace Walter.Plugins.SurveyPlugin
{
    public class Survey : Module
    {
        #region tables and columns
        public static string TablesSurveys = "survey_surveys";
        public static string TablesSurveysId = "Id";
        public static string TablesSurveysTopic = "Topic";
        public static string TablesSurveysCreatedBy = "CreatedBy";
        public static string TablesSurveysCreatedByUniqueId = "CreatedByIdent";
        public static string TablesSurveysCreatedOn = "CreatedOn";
        public static string TablesSurveysEndDate = "EndDate";

        public static string TablesOptions = "survey_options";
        public static string TablesOptionsId = "Id";
        public static string TablesOptionsOption = "Option";
        public static string TablesOptionsSurveyId = "SurveyId";

        public static string TablesVotes = "survey_votes";
        public static string TablesVotesId = "Id";
        public static string TablesVotesSurveyId = "SurveyId";
        public static string TablesVotesOptionId = "OptionId";
        public static string TablesVotesUser = "User";
        public static string TablesVotesUserUniqueId = "UserIdent";
        public static string TablesVotesCreatedOn = "CreatedOn";
        #endregion

        public override string[] GetCommand()
        {
            return new string[]
                       {
                           "!startpoll", "!startsurvey", "!polls", "!surveys", "!vote", "!surveyhelp", "!pollhelp",
                           "!pollachiv", "!surveyachiv", "!poll", "!survey"
                       };
        }

        public override void Init()
        {
            Flags = ModuleFlags.UseChannelMessageEvent | ModuleFlags.UseQueryMessageEvent;
            CreateTables();
        }

        /// <summary>
        /// Creates the tables.
        /// </summary>
        /// <Author>RC</Author>
        /// <Date>24.01.2012</Date>
        private void CreateTables()
        {
            DatabaseHelper.CreateTable("CREATE TABLE IF NOT EXISTS " + TablesSurveys + " " +
                                       "( " + TablesSurveysId + " INTEGER PRIMARY KEY, " +
                                       TablesSurveysTopic + " VARCHAR(200), " + 
                                       TablesSurveysCreatedBy + " VARCHAR(50), " +
                                       TablesSurveysCreatedByUniqueId + " VARCHAR(200), " +
                                       TablesSurveysCreatedOn + " DATETIME, " +
                                       TablesSurveysEndDate + " DATETIME);");

            //create survey options table
            DatabaseHelper.CreateTable("CREATE TABLE IF NOT EXISTS " + TablesOptions + " " +
                                       "( " + TablesOptionsId + " INTEGER PRIMARY KEY, " +
                                       TablesOptionsOption + " VARCHAR(200), " +
                                       TablesOptionsSurveyId + " INTEGER)");

            //create survey vote table
            DatabaseHelper.CreateTable("CREATE TABLE IF NOT EXISTS " + TablesVotes + " " +
                                       "( " + TablesVotesId + " INTEGER PRIMARY KEY, " +
                                       TablesVotesSurveyId + " INTEGER, " +
                                       TablesVotesOptionId + " INTEGER, " +
                                       TablesVotesUser + " VARCHAR(200), " +
                                       TablesVotesUserUniqueId + " VARCHAR(200), " +
                                       TablesVotesCreatedOn + " DATETIME)");
        }

        public override void OnChannelMessage(Meebey.SmartIrc4net.IrcMessageData data)
        {
            if (data.Message.StartsWith("!startpoll ") || data.Message.StartsWith("!startsurvey "))
            {
                //Create a new survey
                HandleSurveyCreation(data);
            }
            else if (data.Message.StartsWith("!surveys") || data.Message.StartsWith("!polls"))
            {
                //send the user all active polls
                ReplyAllOpenSurveys(data);
            }
            else if (data.Message.StartsWith("!vote"))
            {
                //vote
                HandleVoting(data);
            }
            else if (data.Message.StartsWith("!survey") || data.Message.StartsWith("!poll"))
            {
                GetSurveyInformations(data);
            }
            else if (data.Message.StartsWith("!surveyarchiv") || data.Message.StartsWith("!pollarchiv"))
            {
                //get all polls ever made !ATTENTION: BIG DATA INCOME!
                BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick, "NotImplementedException");
            }
        }

        private void HandleVoting(IrcMessageData data)
        {
            string[] command = data.Message.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries);
            if (command.Length == 3)
            {
                int surveyId;
                int optionId;

                try
                {
                    surveyId = Convert.ToInt32(command[1]);
                    optionId = Convert.ToInt32(command[2]);
                }
                catch (Exception)
                {
                    BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                     "Du hast da wohl etwas falsch gemacht." +
                                                     " Nutze '!vote #id #option' und ersetze " +
                                                     "#id mit der Umfrage-Id und #option mit der Antwort-Id." +
                                                     " Errorcode: voteWrongValue");

                    return;
                }
				
#if MONO
                SqliteDataReader reader =
                    DatabaseHelper.DataRequest("SELECT * FROM " + TablesVotes + " WHERE " + TablesVotesSurveyId + " = " +
                                               surveyId + " AND " + TablesVotesUserUniqueId + " = '" + data.From + "'");
#else
				SQLiteDataReader reader =
                    DatabaseHelper.DataRequest("SELECT * FROM " + TablesVotes + " WHERE " + TablesVotesSurveyId + " = " +
                                               surveyId + " AND " + TablesVotesUserUniqueId + " = '" + data.From + "'");
#endif

                //if the user already voted for this survey tell him and cancel the process.
                if (reader.HasRows)
                {
                    BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                     "Du hast für diese Umfrage schon abgestimmt.");
                    return;
                }

                int result =
                    DatabaseHelper.ExecuteRequest("INSERT INTO " + TablesVotes + " ( " + TablesVotesUser + ", " +
                                                  TablesVotesUserUniqueId + ", " + TablesVotesSurveyId + ", " +
                                                  TablesVotesOptionId + ", " + TablesVotesCreatedOn + " ) VALUES ( '" +
                                                  data.Nick + "', '" + data.From + "', " + surveyId.ToString() + ", " +
                                                  optionId + ", '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") +
                                                  "' )");

                if (result != -1)
                {
                    BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick, "Deine Stimme wurde gezählt.");
                }
                else
                {
                    BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                 "Du hast da wohl etwas falsch gemacht." +
                                 " Nutze '!vote #id #option' und ersetze " +
                                 "#id mit der Umfrage-Id und #option mit der Antwort-Id." +
                                 " Errorcode: voteInsertFailed");
                }
            }
            else
            {
                BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                 "Du hast da wohl etwas falsch gemacht." +
                                                 " Nutze '!vote #id #option' und ersetze " +
                                                 "#id mit der Umfrage-Id und #option mit der Antwort-Id." +
                                                 " Errorcode: voteWrongSyntax");
            }
        }

        /// <summary>
        /// Gets the survey informations and sends them to the user
        /// </summary>
        /// <param name="data">The data.</param>
        /// <Author>RC</Author>
        /// <Date>30.01.2012</Date>
        private void GetSurveyInformations(IrcMessageData data)
        {
            string[] command = data.Message.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries);
            if (command.Length == 2)
            {
                int surveyId = -1;
                try
                {
                    surveyId = Convert.ToInt32(command[1]);
                }
                catch (Exception)
                {
                    //if the user didn't entered a valid integer value send a message and stop this process.
                    BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                 "Du hast da wohl etwas falsch gemacht. Nutze '!poll #'" +
                                                 " und ersetze die # ( Raute ) durch die Umfrage-Id. Errorcode: pollNoValidInt");
                    return;
                }

#if MONO
                SqliteDataReader reader =
#else
				SQLiteDataReader reader =
#endif
                    DatabaseHelper.DataRequest("SELECT * FROM " + TablesSurveys + " WHERE " + TablesSurveysId + " = " +
                                               surveyId);

                if (reader.HasRows)
                {
                    reader.Read();

                    string topic = reader[TablesSurveysTopic].ToString();
                    DateTime endDate = DateTime.Parse(reader[TablesSurveysEndDate].ToString());
                    DateTime createdOn = DateTime.Parse(reader[TablesSurveysCreatedOn].ToString());
                    string createdBy = reader[TablesSurveysCreatedBy].ToString();

                    //disconnect to clear the reader and to create new queries
                    DatabaseHelper.Disconnect();

                    reader =
                        DatabaseHelper.DataRequest("SELECT * FROM " + TablesOptions + " WHERE " + TablesOptionsSurveyId +
                                                   " = " + surveyId);

                    //send the user th following informations
                    BotMain.Instance.IRC.SendMessage(SendType.Message, data.Nick,
                                                     "Informationen zur Umfrage: " + surveyId.ToString() +
                                                     " mit dem Titel '" + topic + "', erstellt von '" + createdBy +
                                                     "' am " + createdOn.ToString("dd.MM.yyyy") + " um " +
                                                     createdOn.ToString("hh:mm") + "Uhr. Diese Umfrage endet in " +
                                                     (endDate.Subtract(createdOn)).Days == "0"
                                                         ? " heute."
                                                         : ((endDate.Subtract(createdOn)).Days + " Tagen"));

                    BotMain.Instance.IRC.SendMessage(SendType.Message, data.Nick, "Folgende Optionen stehen dir zur Verfügung.");

                    //if there are no options
                    if (!reader.HasRows)
                        BotMain.Instance.IRC.SendMessage(SendType.Message, data.Nick,
                                                         "Es stehen dir leider keine Optionen zur Verfügung.");

                    while (reader.Read())
                    {
                        BotMain.Instance.IRC.SendMessage(SendType.Message, data.Nick,
                                                         "ID: " + reader[TablesOptionsId].ToString() + " | " +
                                                         reader[TablesOptionsOption].ToString());
                    }

                    //just close the connection, why should it still open?
                    DatabaseHelper.Disconnect();
                }
                else
                {
                    //if the user didn't entered a valid integer value send a message and stop this process.
                    BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                 "Du hast da wohl etwas falsch gemacht. Nutze '!poll #'" +
                                                 " und ersetze die # ( Raute ) durch die Umfrage-Id. Errorcode: pollNoRows");
                }

            }
            else
            {
                BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                 "Du hast da wohl etwas falsch gemacht. Nutze '!poll #'" +
                                                 " und ersetze die # ( Raute ) durch die Umfrage-Id. Errorcode: pollWrongSyntax");
            }
        }

        /// <summary>
        /// Replies all open surveys.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <Author>RC</Author>
        /// <Date>30.01.2012</Date>
        private void ReplyAllOpenSurveys(IrcMessageData data)
        {
#if MONO
            SqliteDataReader reader =
#else
			SQLiteDataReader reader =
#endif
                DatabaseHelper.DataRequest("SELECT * FROM " + TablesSurveys + " WHERE EndDate > '" +
                                           DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "'");

            while (reader.Read())
            {
                BotMain.Instance.IRC.SendMessage(SendType.Message, data.Nick,
                                                 "ID: " + reader[TablesSurveysId].ToString() + " Topic: " +
                                                 reader[TablesSurveysTopic]);
            }
        }

        /// <summary>
        /// Handles the survey creation.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <Author>RC</Author>
        /// <Date>24.01.2012</Date>
        private void HandleSurveyCreation(IrcMessageData data)
        {
            string[] commands = data.Message.Split(new string[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
            if (commands.Length > 10)
            {
                BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                 "Du hast zu viele Antwortmöglichkeiten angegeben, daher wurde diese Umfrage " +
                                                 "nicht weiter bearbeitet.");
                return;
            }

            if (commands.Length == 3)
            {
                //this seems to be a "yes/no" survey
                int result = CreateNewPoll(commands[2], data.Nick, data.From, commands[1]);

                if (result != -1)
                {
                    BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                     "Deine Umfrage wurde mit der ID: " + result.ToString() +
                                                     " angelegt. Wenn du das Ergebnis erfahren möchtest schreibe '!poll " +
                                                     result.ToString() + "' oder '!survey " + result.ToString() +
                                                     "'.");
                }
                else
                {
                    BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                     "Es ist ein Fehler aufgetreten. Deine Umfrage wurde nicht gespeichert.");
                }
            }
            else if (commands.Length > 5)
            {
                //this seems to be a survey with at least 2 vote options
                DatabaseHelper.ExecuteRequest("INSERT INTO " + TablesSurveys + " ( " + TablesSurveysTopic + ", " +
                                          TablesSurveysCreatedBy + ", " + TablesSurveysCreatedByUniqueId + ", " +
                                          TablesSurveysCreatedOn + ", " +
                                          TablesSurveysEndDate + " ) VALUES ( '" + commands[2].Trim() +
                                          "', '" + data.Nick + "', '" + data.From + "', '" +
                                          DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "', + '" +
                                          DateTime.Parse(commands[1]).ToString("yyyy-MM-dd hh:mm:ss") + "' );");

                int surveyId = DatabaseHelper.GetLastInsertedId();

                for (int i = 3; i < commands.Length; i++)
                {
                    DatabaseHelper.ExecuteRequest("INSERT INTO " + TablesOptions + " ( " + TablesOptionsOption + ", " +
                                              TablesOptionsSurveyId + " ) VALUES ( '" + commands[i] + "', " + surveyId.ToString() + " )");
                }

            }
            else
            {
                //woopsy, seems that someone did something wrong
                BotMain.Instance.IRC.SendMessage(SendType.Notice, data.Nick,
                                                 "Ich denke du hast eine Falsche eingabe getätigt. Überprüfe noch mal einen Befehl: '" +
                                                 data.Message + "'.");
            }
        }

        /// <summary>
        /// Creates a new Yes / No Poll. Returns the survey id or -1 if anything fails.
        /// </summary>
        /// <param name="topic">The topic.</param>
        /// <param name="createdBy">The created by.</param>
        /// <param name="endDate">The created by.</param>
        /// <returns></returns>
        /// <Author>RC</Author>
        /// <Date>24.01.2012</Date>
        private int CreateNewPoll(string topic, string createdBy, string uniqueUserId, string endDate)
        {
            DatabaseHelper.ExecuteRequest("INSERT INTO " + TablesSurveys + " ( " + TablesSurveysTopic + ", " +
                                          TablesSurveysCreatedBy + ", " + TablesSurveysCreatedByUniqueId + ", " +
                                          TablesSurveysCreatedOn + ", " +
                                          TablesSurveysEndDate + " ) VALUES ( '" + topic.Trim() +
                                          "', '" + createdBy + "', '" + uniqueUserId + "', '" +
                                          DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "', + '" +
                                          DateTime.Parse(endDate).ToString("yyyy-MM-dd hh:mm:ss") + "' );");

            int surveyId = DatabaseHelper.GetLastInsertedId();

            if (surveyId != -1)
            {
                //insert YES / NO options
                DatabaseHelper.ExecuteRequest("INSERT INTO " + TablesOptions + " ( " + TablesOptionsOption + ", " +
                                              TablesOptionsSurveyId + " ) VALUES ( 'Ja', " + surveyId.ToString() + " )");

                DatabaseHelper.ExecuteRequest("INSERT INTO " + TablesOptions + " ( " + TablesOptionsOption + ", " +
                                              TablesOptionsSurveyId + " ) VALUES ( 'Nein', " + surveyId.ToString() +
                                              " )");
            }

            return surveyId;
        }
    }
}