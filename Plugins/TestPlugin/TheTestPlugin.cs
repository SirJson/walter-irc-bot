﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Walter.Core;
using Meebey.SmartIrc4net;

namespace TestPlugin
{
    public class TheTestPlugin : Module
    {
        public override void Init()
        {
            Flags = ModuleFlags.UseChannelMessageEvent | ModuleFlags.UseUpdateEvent;
        }

        public override void OnUpdate()
        {
            base.OnUpdate();
        }

        public override void OnChannelMessage(IrcMessageData data)
        {
            if (data.Message == "The quick brown fox jumps over the lazy sheep")
                BotMain.Instance.IRC.SendMessage(SendType.Message, data.Channel, "As you wish sir");
        }

        public override string[] GetCommand()
        {
            return new string[] { "global" };
        }
    }
}
