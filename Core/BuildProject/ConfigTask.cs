﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml;
using Microsoft.Build.Framework;

namespace BuildProject
{
    public class ConfigTask : ITask
    {
        #region ITask Members

        private IBuildEngine _buildEngine;
        private ITaskHost _hostObject;

        public IBuildEngine BuildEngine
        {
            get
            {
                return _buildEngine;
            }
            set
            {
                _buildEngine = value;
            }
        }

        [Required]
        public ITaskItem[] Plugins { get; set; }

        [Required]
        public string Filename { get; set; }

        [Required]
        public string Server { get; set; }

        [Required]
        public string Port { get; set; }

        [Required]
        public string Nickname { get; set; }

        public string[] Channels { get; set; }

        public bool Execute()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("IrcBot");
                XmlElement ele = doc.CreateElement("Server");
                ele.SetAttribute("address", Server);
                ele.SetAttribute("port", Port);
                root.AppendChild(ele);

                ele = doc.CreateElement("User");
                ele.SetAttribute("nick", Nickname);
                root.AppendChild(ele);

                foreach (string channel in Channels)
                {
                    ele = doc.CreateElement("Channel");
                    ele.SetAttribute("name", channel);
                    root.AppendChild(ele);
                }

                foreach (var taskItem in Plugins)
                {
                    ele = doc.CreateElement("Module");
                    ele.SetAttribute("name", "Plugins\\\\" + taskItem.GetMetadata("Filename") + "\\\\" + taskItem.GetMetadata("Filename") + ".dll");
                    root.AppendChild(ele);
                }

                doc.AppendChild(root);
                doc.Save(Filename);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public ITaskHost HostObject
        {
            get
            {
                return _hostObject;
            }
            set
            {
                _hostObject = value;
            }
        }

        #endregion
    }
}
