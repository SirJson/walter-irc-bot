﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace Walter.Core
{
    class Config
    {
        public string ServerAddress, Nickname;
        public int Port;
        public List<string> Channels, Modules;

        public Config()
        {
            Channels = new List<string>();
            Modules = new List<string>();
        }

        public void Load(string xmlFile)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlElement root = doc.DocumentElement;

            foreach (XmlNode daten in root.ChildNodes)
            {
                switch (daten.Name)
                {
                    case "Server":
                        ServerAddress = daten.Attributes["address"].InnerText;
                        Port = Convert.ToInt32(daten.Attributes["port"].InnerText);
                        break;
                    case "User":
                        Nickname = daten.Attributes["nick"].InnerText;
                        break;
                    case "Channel":
                        Channels.Add(daten.Attributes["name"].InnerText);
                        break;
                    case "Module":
                        Modules.Add(daten.Attributes["name"].InnerText);
                        break;
                }
            }
        }
    }
}