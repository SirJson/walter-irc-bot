﻿/**********************************************************************
 * BotMain Version 0.2
 **********************************************************************
 * Author: Jeason
 **********************************************************************
 * History
 *      Kato    v0.2  -> fixed some startup crashes and fixed flags
 *      Kato    v0.1c -> added some irc events
 *      Kato    v0.1b -> added command parameter support
 *      Kato    v0.1a -> added multi command per plugin support
 */

namespace Walter.Core
{
	using System;
	using System.Collections.Generic;
	using System.Text;
	using Meebey.SmartIrc4net;
	using System.Reflection;
	using System.Linq;

	public class BotMain
	{
		#region fields
		public readonly IrcClient IRC = new IrcClient();
		private Config config = new Config();
		private bool running = true;
		private LogWriter writer;

		public static readonly BotMain Instance = new BotMain();

		SortedList<string, Module> ModuleList = new SortedList<string, Module>();
		List<Module> GlobalModuleList = new List<Module>();

		/// <summary>
		/// Gets a value indicating whether the bot is running.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is running; otherwise, <c>false</c>.
		/// </value>
		/// <creationDate>22.01.2012 01:30</creationDate>
		public bool IsRunning
		{
			get { return running; }
		}

		/// <summary>
		/// Gets the name of the bot.
		/// </summary>
		/// <value>The name of the bot.</value>
		/// <Date>23.01.2012</Date>
		public string BotName
		{
			get { return config.Nickname; }
		}

		#endregion

		/// <summary>
		/// Inits the specified modules.
		/// </summary>
		/// <creationDate>22.01.2012 01:23</creationDate>
		public void Init()
		{
			Module moduleObject;
			string[] modules;

			//init log writer
			writer = new LogWriter(".");
			writer.WriteMessage("Walter IRC Bot");
			writer.WriteMessage("Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
			writer.WriteMessage("Init DatabaseHelper");

			//init database
			DatabaseHelper.Init("Database.db");
			//init config
			config.Load("bot.cfg");
			writer.WriteMessage("Loading Modules");
			

#if DEBUG
			modules = new string[] { "Plugins\\SurveyPlugin\\SurveyPlugin.dll" };
#else
			modules = config.Modules.ToArray();
#endif

			//get commands for every preloaded module
			foreach (string module in modules)
			{
				try
				{
					moduleObject = this.LoadModule<Module>(module);
					foreach (string item in moduleObject.GetCommand())
					{
						if (string.Equals(item, "global"))
							GlobalModuleList.Add(moduleObject);
						else
							ModuleList.Add(item, moduleObject);
					}
					moduleObject.Init();
				}
				catch (Exception ex)
				{
					throw new Exception("An error occurred while loading module: " + module, ex);
				}
			}


			IRC.Encoding = Encoding.UTF8;
			IRC.SendDelay = 200;
			IRC.ActiveChannelSyncing = true;

			//init irc events
			IRC.OnQueryMessage += IRC_OnQueryMessage;
			IRC.OnError += IRC_OnError;
			IRC.OnRawMessage += IRC_OnRawMessage;
			IRC.OnChannelMessage += IRC_OnChannelMessage;
			IRC.OnJoin += IRC_OnJoin;
			IRC.OnNickChange += IRC_OnNickChange;

			writer.WriteMessage("Connect to " + config.ServerAddress);
			IRC.Connect(config.ServerAddress, config.Port);
			writer.WriteMessage("Login as " + config.Nickname);
			IRC.Login(config.Nickname, config.Nickname + "_Bot");

			foreach (string channel in config.Channels)
			{
				IRC.RfcJoin(channel);
			}
		}

		/// <summary>
		/// Calls every module which registered for the irc update event.
		/// </summary>
		/// <creationDate>22.01.2012 01:29</creationDate>
		public void Update()
		{
			foreach (var module in GlobalModuleList)
			{
				if ((module.Flags & ModuleFlags.UseUpdateEvent) == ModuleFlags.UseUpdateEvent)
				{
					module.OnUpdate();
				}
			}
			IRC.ListenOnce();
		}

		private T LoadModule<T>(string file)
		{
			Assembly assembly = Assembly.LoadFrom(file);
			Type[] types = assembly.GetTypes();
			object result = null;
			foreach (Type type in types)
			{
				if (typeof(T).IsAssignableFrom(type))
				{
					result = assembly.CreateInstance(type.ToString());
				}
			}
			if (result == null)
				throw new Exception("Could not find " + typeof(T).Name + " in " + file);
			return (T)result;
		}

		#region irc events


		void IRC_OnNickChange(object sender, NickChangeEventArgs e)
		{
			foreach (var module in ModuleList.Where(o => (o.Value.Flags & ModuleFlags.UseOnNickChangeEvent) == ModuleFlags.UseOnNickChangeEvent))
				module.Value.OnNickChange(e);

			foreach (var module in GlobalModuleList.Where(o => (o.Flags & ModuleFlags.UseOnNickChangeEvent)== ModuleFlags.UseOnNickChangeEvent))
				module.OnNickChange(e);
		}

		void IRC_OnJoin(object sender, JoinEventArgs e)
		{
			if (e.Who != config.Nickname)
			{
				foreach (var module in ModuleList.Where(o => (o.Value.Flags & ModuleFlags.UseOnJoinEvent) == ModuleFlags.UseOnJoinEvent))
					module.Value.OnJoin(e);

				foreach (var module in GlobalModuleList.Where(o => (o.Flags & ModuleFlags.UseOnJoinEvent) == ModuleFlags.UseOnJoinEvent))
					module.OnJoin(e);
			}
		}

		void IRC_OnChannelMessage(object sender, IrcEventArgs e)
		{
			string possibleChannelCommand = e.Data.Message.Split(new string[] { " " }, StringSplitOptions.None)[0];
			foreach (var module in ModuleList.Where(o => (o.Value.Flags & ModuleFlags.UseChannelMessageEvent)== ModuleFlags.UseChannelMessageEvent && o.Key == possibleChannelCommand))
			{
				module.Value.OnChannelMessage(e.Data);
			}

			foreach (var module in GlobalModuleList.Where(o => (o.Flags & ModuleFlags.UseChannelMessageEvent) == ModuleFlags.UseChannelMessageEvent))
			{
				module.OnChannelMessage(e.Data);
			}
		}

		void IRC_OnQueryMessage(object sender, IrcEventArgs e)
		{
			//srsly? stop running on a query?
			//if (e.Data.Message == "exit")
			//{
			//    this.running = false;
			//}

			string possibleChannelCommand = e.Data.Message.Split(new string[] { " " }, StringSplitOptions.None)[0];
			foreach (var module in ModuleList.Where(o => (o.Value.Flags & ModuleFlags.UseQueryMessageEvent)== ModuleFlags.UseQueryMessageEvent && o.Key == possibleChannelCommand))
			{
				module.Value.OnQueryMessage(e.Data);
			}

			foreach (var module in GlobalModuleList.Where(o => (o.Flags & ModuleFlags.UseQueryMessageEvent) == ModuleFlags.UseQueryMessageEvent))
			{
				module.OnQueryMessage(e.Data);
			}
		}

		void IRC_OnError(object sender, ErrorEventArgs e)
		{
			writer.WriteMessage("Error: " + e.ErrorMessage);
		}

		void IRC_OnRawMessage(object sender, IrcEventArgs e)
		{
			writer.WriteMessage(e.Data.RawMessage);
		}

		#endregion
	}
}
