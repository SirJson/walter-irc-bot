﻿/****************************************************
 * DatabaseHelper Version 0.1a
 ****************************************************
 * Author: Kato
 * Copyright: I don't care if you use it =)
 ***************************************************
 * Usage:
 * The whole class is designed for common static use
 * First of all you have to initialize the helper by 
 * calling the DatabaseHelper.Init("C:\Path\To\Database.db")
 * method. After this you can use every other public function
 * for questions join irc.euirc.net #xna and ask for help =)
 */

namespace Walter.Core
{
    using System;
    using System.Data;
#if MONO
    using Mono.Data.Sqlite;
#else
	using System.Data.SQLite;
#endif
    using System.Data.Common;

    /// <summary>
    /// 
    /// </summary>
    /// <author>Roy "Kato" Carlitscheck (RC) </author>
    /// <copyright>ek-Dev</copyright>
    /// <creationDate>21.01.2012 21:26</creationDate>
    public static class DatabaseHelper
    {
        #region fields

        private static string _connectionString = string.Empty;
#if MONO
        private static SqliteConnection _connection;
#else
		private static SQLiteConnection _connection;
#endif

        #endregion

        #region public methods

        /// <summary>
        /// Creates a table.
        /// </summary>
        /// <param name="sqlCommand">The SQL command.</param>
        /// <author>Roy "Kato" Carlitscheck (RC) </author>
        /// <copyright>ek-Dev</copyright>
        /// <creationDate>21.01.2012 22:36</creationDate>
        public static void CreateTable(string sqlCommand)
        {
            if (sqlCommand == string.Empty)
                throw new ArgumentNullException("sqlCommand");

#if MONO
			SqliteCommand command;
#else
            SQLiteCommand command;
#endif

            try
            {
                if (Connect())
                {
#if MONO
                    command = new SqliteCommand(sqlCommand, _connection);
#else
					command = new SQLiteCommand(sqlCommand, _connection);
#endif
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while creating a table", ex);
            }
            finally
            {
                Disconnect();
            }
        }

        /// <summary>
        /// Inits the database helper with the given connection string
        /// </summary>
        /// <param name="filename">name of the file</param>
        /// <author>Roy "Kato" Carlitscheck (RC) </author>
        /// <copyright>ek-Dev</copyright>
        /// <creationDate>21.01.2012 21:40</creationDate>
        public static void Init(string filename)
        {
            if (filename == string.Empty)
                throw new ArgumentNullException("filename");
#if MONO
            _connection = new SqliteConnection();
#else
			_connection = new SQLiteConnection();
#endif
            _connection.ConnectionString = new DbConnectionStringBuilder()
                                                {
                                                    {"Data Source", filename},
                                                    {"Version", "3"},
                                                    {"FailIfMissing", "False"},
                                                }.ConnectionString;
        }

        /// <summary>
        /// Datas the request.
        /// </summary>
        /// <param name="sqlCommand">The SQL command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <author>Roy "Kato" Carlitscheck (RC) </author>
        /// <copyright>ek-Dev</copyright>
        /// <creationDate>21.01.2012 22:12</creationDate>
       	
#if MONO
		public static SqliteDataReader DataRequest(string sqlCommand, params object[] parameters)
#else
        public static SQLiteDataReader DataRequest(string sqlCommand, params object[] parameters)
#endif
        {
#if MONO
            SqliteCommand command;
            SqliteDataReader reader;
#else
			SQLiteCommand command;
            SQLiteDataReader reader;
#endif

            try
            {
                if (Connect())
                {
#if MONO
                    command = new SqliteCommand(sqlCommand, _connection);
#else
					command = new SQLiteCommand(sqlCommand, _connection);
#endif
                    command.Parameters.AddRange(parameters);

                    reader = command.ExecuteReader();
                    return reader;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred", ex);
            }
        }

        /// <summary>
        /// Gets the last inserted id. Returns -1 if an error occurred or no row was inserted.
        /// </summary>
        /// <returns></returns>
        /// <Author>RC</Author>
        /// <Date>30.01.2012</Date>
        public static int GetLastInsertedId()
        {
#if MONO
            SqliteCommand command;
#else
			SQLiteCommand command;
#endif

            try
            {
                if (Connect())
                {
#if MONO
                    command = new SqliteCommand("SELECT last_insert_rowid();", _connection);
#else
					command = new SQLiteCommand("SELECT last_insert_rowid();", _connection);
#endif
                    return Convert.ToInt32(command.ExecuteScalar());
                }

                return -1;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        /// <summary>
        /// Requests to send a INSERT 
        /// </summary>
        /// <param name="sqlCommand">The SQL command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        /// <author>Roy "Kato" Carlitscheck (RC) </author>
        /// <copyright>ek-Dev</copyright>
        /// <creationDate>21.01.2012 22:46</creationDate>
        public static int ExecuteRequest(string sqlCommand, params object[] parameters)
        {
#if MONO
            SqliteCommand command;
#else
			SQLiteCommand command;
#endif

            try
            {
                if (Connect())
                {
#if MONO
                    command = new SqliteCommand(sqlCommand, _connection);
#else
					command = new SQLiteCommand(sqlCommand, _connection);
#endif
                    command.Parameters.AddRange(parameters);

                    return command.ExecuteNonQuery();
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while inserting data", ex);
            }
        }

        /// <summary>
        /// Disconnects this instance.
        /// </summary>
        /// <author>Roy "Kato" Carlitscheck (RC) </author>
        /// <copyright>ek-Dev</copyright>
        /// <creationDate>21.01.2012 22:17</creationDate>
        public static void Disconnect()
        {
            if (_connection != null)
            {
                switch (_connection.State)
                {
                    case ConnectionState.Open:
                        _connection.Close();
                        break;
                    case ConnectionState.Closed:
                        break;
                    default:
                        throw new Exception("Connection is still in use or broken.");
                }
            }
        }

        #endregion

        #region private methods

        /// <summary>
        /// Connects to the database.
        /// </summary>
        /// <returns></returns>
        /// <author>Roy "Kato" Carlitscheck (RC) </author>
        /// <copyright>ek-Dev</copyright>
        /// <creationDate>21.01.2012 21:52</creationDate>
        private static bool Connect()
        {
            if(_connection == null)
                throw new Exception("Have you forgot to run the Init method? ;)");

            if (_connection.State == ConnectionState.Open)
                return true;

            switch (_connection.State)
            {
                case ConnectionState.Closed:

                    try
                    {
                        _connection.Open();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("An error occurred while connection to the database.", ex);
                    }

                    return true;

                default:
                    return false;
            }
        }

        #endregion
    }
}