﻿/**********************************************************************
 * Module Version 0.2
 **********************************************************************
 * Author: Jeason
 **********************************************************************
 * History
 *      Kato    v0.2  -> fixed flags issue
 *      Kato    v0.1a -> added some irc events
 */

namespace Walter.Core
{
    using Meebey.SmartIrc4net;

    public enum ModuleFlags
    {
        UseQueryMessageEvent = 0x00,
        UseChannelMessageEvent = 0x01,
        UseOnJoinEvent = 0x02,
        UseUpdateEvent = 0x04,
        UseOnNickChangeEvent = 0x08
    }

    /// <summary>
    /// Represents a Plugin.
    /// </summary>
    /// <creationDate>22.01.2012 01:28</creationDate>
    public class Module
    {
        /// <summary>
        /// Called when recieving a private message.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <creationDate>22.01.2012 01:26</creationDate>
        public virtual void OnQueryMessage(IrcMessageData data) {}
        
        /// <summary>
        /// Called when recieving a channel message
        /// </summary>
        /// <param name="data">The data.</param>
        /// <creationDate>22.01.2012 01:26</creationDate>
        public virtual void OnChannelMessage(IrcMessageData data) {}

        /// <summary>
        /// Called when someone joins a channel.
        /// </summary>
        /// <param name="data">The <see cref="Meebey.SmartIrc4net.JoinEventArgs"/> instance containing the event data.</param>
        /// <Author>Kato</Author>
        /// <Date>23.01.2012</Date>
        public virtual void OnJoin(JoinEventArgs data) {}

        /// <summary>
        /// Called when someone changes his name
        /// </summary>
        /// <param name="data">The <see cref="Meebey.SmartIrc4net.NickChangeEventArgs"/> instance containing the event data.</param>
        /// <Author>Kato</Author>
        /// <Date>23.01.2012</Date>
        public virtual void OnNickChange(NickChangeEventArgs data){}

        /// <summary>
        /// Inits this instance.
        /// </summary>
        /// <creationDate>22.01.2012 01:27</creationDate>
        public virtual void Init() { }
       
        /// <summary>
        /// Called when the irc client updates
        /// </summary>
        /// <creationDate>22.01.2012 01:27</creationDate>
        public virtual void OnUpdate() { }
       
        /// <summary>
        /// Gets the commands.
        /// </summary>
        /// <returns></returns>
        /// <creationDate>22.01.2012 01:27</creationDate>
        public virtual string[] GetCommand()
        {
            return new string[] {"global"};
        }

        public ModuleFlags Flags = 0;
    }
}
