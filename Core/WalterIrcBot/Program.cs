﻿using System;
using System.Collections.Generic;
using System.Text;
using Walter.Core;

namespace Walter
{
    class Program
    {
        static void Main(string[] args)
        {
            BotMain bot = BotMain.Instance;
            bot.Init();
            while (bot.IsRunning)
            {
                bot.Update();
            }
        }
    }
}
